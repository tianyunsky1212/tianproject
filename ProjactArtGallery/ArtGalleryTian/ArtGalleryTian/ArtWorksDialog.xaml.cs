﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ArtGalleryTian.ArtWork;
using System.Drawing;
using Microsoft.Win32;



namespace ArtGalleryTian
{
    /// <summary>
    /// Interaction logic for ArtWorksDialog.xaml
    /// </summary>
    public partial class ArtWorksDialog : Window
    {


        public string imagePath;

        public ArtWorksDialog(Window parent, ArtWork __currArtWork = null)
        {
            InitializeComponent();
            Owner = parent;
            Globals.currArtWork = __currArtWork;

            if (Globals.currArtWork != null)
            {
                lblId.Content = Globals.currArtWork.ArtWorkId + "";
                tbArtWorkName.Text = Globals.currArtWork.ArtWorkName;

                tbPrice.Text = Globals.currArtWork.ArtWorkPrice.ToString();

                dpCreateDate.SelectedDate = Globals.currArtWork.ArtWorkDate;

                cbxCategory.Text = Globals.currArtWork.CategoryName.ToString();
                tbArtWorkDes.Text = Globals.currArtWork.ArtWorkDescription;
                bnSave.Content = "Edit";
                cmbArtistName.Text = Globals.currArtWork.Artist.ArtistName.ToString();
                tbArtistDes.Text = Globals.currArtWork.Artist.ArtistDescription.ToString();



                MemoryStream memoryStream = new MemoryStream(Globals.currArtWork.ArtWorkImage);
                // Create source
                BitmapImage bitmapImage = new BitmapImage();
                // BitmapImage.UriSource must be in a BeginInit/EndInit block.
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
                // Set the image source
                imageArtWork.Source = bitmapImage;
                imagePath = imageArtWork.Source.ToString();


            }

            else bnSave.Content = "Add";
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            string artworkname = tbArtWorkName.Text;

            decimal artworkprice = decimal.Parse(tbPrice.Text);
            DateTime artworkcreatedate = dpCreateDate.SelectedDate.Value;
            int year = artworkcreatedate.Year;
            string artworkdescription = tbArtWorkDes.Text;
            string catStr = cbxCategory.Text;
            ArtWork.CategoryEnum category;
            if (!Enum.TryParse<ArtWork.CategoryEnum>(catStr, out category))
            {
                MessageBox.Show("Internal error: enum value invalid " + catStr);
                return;
            }

            string artStr = cmbArtistName.Text;
            Artist.ArtistEnum artistname;
            if (!Enum.TryParse<Artist.ArtistEnum>(artStr, out artistname))
            {
                MessageBox.Show(" error: enum value invalid " + artStr);
                return;
            }
            string artisdes = tbArtistDes.Text;

            //add image
            MemoryStream memoryStream = new MemoryStream();
            //from stream to JpegBitmapencoder
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            //to frame that can accept the data format
            encoder.Frames.Add(BitmapFrame.Create(imageArtWork.Source as BitmapImage));
            encoder.Save(memoryStream);
            byte[] artworkImage = memoryStream.ToArray();
           

            if (Globals.currArtWork == null)
            {

                ArtWork p = new ArtWork()
                {
                    ArtWorkName = artworkname,
                    ArtWorkPrice = artworkprice,
                    ArtWorkDate = artworkcreatedate,
                    ArtWorkDescription = artworkdescription,
                    CategoryName = category,
                    ArtWorkImage = artworkImage,
                };

                Artist a = new Artist()
                {
                    ArtistName = artistname,
                    ArtistDescription = artisdes,

                };
                p.ArtistId = a.ArtistId;
                Globals.ctx.ArtWorks.Add(p);
                Globals.ctx.Artists.Add(a);
                Globals.ctx.SaveChanges();
            }


            else
            {
                Globals.currArtWork.ArtWorkName = artworkname;
                Globals.currArtWork.ArtWorkPrice = artworkprice;
                Globals.currArtWork.ArtWorkDate = artworkcreatedate;
                Globals.currArtWork.ArtWorkDescription = artworkdescription;
                Globals.currArtWork.CategoryName = category;
                if (imagePath == null)
                {
                    Globals.currArtWork.ArtWorkImage = artworkImage;
                }
                Globals.currArtWork.Artist.ArtistName = artistname;
                Globals.currArtWork.Artist.ArtistDescription = artisdes;
                Globals.ctx.SaveChanges();

            }


            MainWindow mw = new MainWindow();
            mw.RefreshList();
            this.DialogResult = true;

        }


        private void bnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void bnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if (op.ShowDialog() == true)
            {
                imagePath = op.FileName;
                imageArtWork.Source = new BitmapImage(new Uri(imagePath));

            }

        }
    }
}




