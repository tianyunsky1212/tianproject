﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace ArtGalleryTian
{
    public  class ArtWork
    {
        [Key]
        public int ArtWorkId { get; set; }

        // one-to-Zero-or-One relationship between ArtWork and Order
        public virtual Order Order { get; set; }

        [MaxLength(50), Required]
        public string ArtWorkName { get; set; }

        [Required]
        public DateTime ArtWorkDate { get; set; }
        

        [MaxLength(500), Required]
        public string ArtWorkDescription { get; set; }

        [Required]
        public byte[] ArtWorkImage { get; set; }
        
        [Required]
        public decimal ArtWorkPrice { get; set; }


        [Required]
        public CategoryEnum CategoryName { get; set; }

        public enum CategoryEnum { Painting = 0, Scrupture = 1, Photography = 2, Installation = 3, Other = 4 }


        // for one to many relationship between Artist and ArtWork
        public int ArtistId { get; set; }
        public Artist Artist { get; set; }
        



        public override string ToString()
        {
            return string.Format("Art Work:id{0} name{1} ,created by {2} on {3}, {4}, {5}",
                ArtWorkId, ArtWorkName, Artist.ArtistName,ArtWorkDate, ArtWorkDescription, ArtWorkPrice.ToString());
        }


        
    }

   
}
