﻿using System.Data.Entity;

namespace ArtGalleryTian
{
    public class ArtGalleryDatabaseContext : DbContext
    {
        public ArtGalleryDatabaseContext() : base("name=artgallerydatabase")
        {
        }
        public DbSet<ArtWork> ArtWorks { get; set; }//add Artworks table
        public DbSet<Artist> Artists { get; set; }//add Artists table
        public DbSet<Order> Orders { get; set; } //add Orders table
        public DbSet<Customer> Customers { get; set; } //add Customers table
    
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Write Fluent API configurations here
            modelBuilder.HasDefaultSchema("ArtGalleryTian");

            // Configure the primary key for the PassPort
            modelBuilder.Entity<Order>()
                .HasKey(t => t.OrderId);

            // Map one-to-zero or one relationship
            modelBuilder.Entity<ArtWork>()
               .HasOptional(s => s.Order) 
               .WithRequired(ord => ord.ArtWork); 
        }
    }
}
//  <!---  connectionString-->
//  <connectionStrings>
//    <add name = "artgallerydatabase"
//    connectionString="Data Source=master123.database.windows.net;
//                      Initial Catalog = master123;
//User ID = master.ad;
//Password=111111Abc;
//                      Connect Timeout = 60; Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
//    providerName="System.Data.SqlClient"/>
//  </connectionStrings>
//  <!---  connectionString-->