﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ArtGalleryTian
{
    /// <summary>
    /// Interaction logic for NewOrder.xaml
    /// </summary>
    public partial class NewOrder : Window
    {
        public NewOrder(Window parent, ArtWork __currArtWork = null, Order __currOrder = null)
        {
            InitializeComponent();
            this.Owner = parent;
            Globals.currArtWork = __currArtWork;
            Globals.currOrder = __currOrder;

            Globals.currArtist = Globals.ctx.Artists.Find(__currArtWork.ArtistId);
            //Globals.currCustomer = Globals.ctx.Customers.Find(__currOrder.CustomerId);

            RefreshOrderDialogCustomerByOrder();
            RefreshOrderDialogArtworkByOrder();

            /*
            if (Globals.currCustomer != null)
            {
                Globals.currOrder = Globals.ctx.Orders.Find(__currCustomer.CustomerId) as Order;
                //Globals.currCustomer = null;
                //Globals.currCustomer = Globals.ctx.Customers.Find(__currArtWork.Order.CustomerId) as Customer;
                lblCustomerName.Content = Globals.currCustomer.CustomerName;
                lblCustomerAddress.Content = Globals.currCustomer.CustomerAddress;
                lblPhone.Content = Globals.currCustomer.CustomerPhone;

            }
            else {
                Globals.currCustomer = Globals.ctx.Customers.Find(__currCustomer.CustomerId) as Customer;
                lblCustomerName.Content = Globals.currCustomer.CustomerName;
                lblCustomerAddress.Content = Globals.currCustomer.CustomerAddress;
                lblPhone.Content = Globals.currCustomer.CustomerPhone;
            }
            */



            // else bnSave.Content = "Add";
            /*
             if (Globals.ctx.Orders.Find(Globals.currCustomer.CustomerId) == null)
             {
                 Globals.currOrder = null;
                 tbCustomerName.Text = Globals.currCustomer.CustomerName;
                 bnSave.Content = "Add";

             }
             else
             {


                 Globals.currCustomer = Globals.ctx.Customers.Find(Globals.currOrder.OrderId) as Customer;
                 tbCustomerName.Text = Globals.currCustomer.CustomerName;
                 tbCustomerAddress.Text = Globals.currCustomer.CustomerAddress;
                 tbCustomerPhone.Text = Globals.currCustomer.CustomerPhone.ToString();

             }

             /*
             if (Globals.currCustomer != null)
             {
                 tbCustomerName.Text = Globals.currCustomer.CustomerName;
                 tbCustomerAddress.Text = Globals.currCustomer.CustomerAddress;
                 tbCustomerPhone.Text = Globals.currCustomer.CustomerPhone.ToString();

             }
             */

            

        }

        private void RefreshOrderDialogArtworkByOrder()
        {
            if (Globals.ctx.Orders.Find(Globals.currArtWork.ArtWorkId) == null)
            {
                Globals.currOrder = null;
                lblOrderArtistName.Content = Globals.currArtist.ArtistName;
                lblOrderArtWorkName.Content = Globals.currArtWork.ArtWorkName;
                lblOrderTotal.Content = Globals.currArtWork.ArtWorkPrice;
                //tbCustomerName.Text = Globals.currCustomer.CustomerName;
                bnSave.Content = "Add";

            }
            else
            {
                Globals.currOrder = Globals.ctx.Orders.Find(Globals.currArtWork.ArtWorkId) as Order;
                Globals.currArtist = Globals.ctx.Artists.Find(Globals.currArtWork.ArtWorkId) as Artist;
                lblOrderArtistName.Content = Globals.currArtist.ArtistName;
                lblOrderArtWorkName.Content = Globals.currArtWork.ArtWorkName;
                lblOrderTotal.Content = Globals.currArtWork.ArtWorkPrice;
                //tbCustomerName.Text = Globals.currOrder.CustomerName;
                tbNotes.Text = Globals.currOrder.Notes;
                dpOrderDate.SelectedDate = Globals.currOrder.OrderDate;
                cbxOrderStatus.Text = Globals.currOrder.Status.ToString();


            }
        }

        private void RefreshOrderDialogCustomerByOrder()
        {
            if (Globals.currOrder != null)
            {
                //Globals.currCustomer = Globals.ctx.Customers.Find(__currOrder.CustomerId);
                Globals.currCustomer = Globals.ctx.Customers.Find(Globals.currOrder.CustomerId);
                lblId.Content = Globals.currOrder.OrderId + "";
                tbCustomerName.Text = Globals.currCustomer.CustomerName;
                tbCustomerAddress.Text = Globals.currCustomer.CustomerAddress;
                tbCustomerPhone.Text = Globals.currCustomer.CustomerPhone.ToString();
                bnSave.Content = "Edit";
            }

            else bnSave.Content = "Add";
        }

        private void bnSave_Click(object sender, RoutedEventArgs e)
        {
            //Globals.currOrder = Globals.ctx.Orders.Find(Globals.currCustomer.CustomerId);
            DateTime orderdate = dpOrderDate.SelectedDate.Value;
            string notes = tbNotes.Text;
            //string customername = tbCustomerName.Text;
            string statusStr = cbxOrderStatus.Text;
            Order.StatusEnum status;
            if (!Enum.TryParse<Order.StatusEnum>(statusStr, out status))
            {
                MessageBox.Show("Internal error: enum value invalid " + statusStr);
                return;
            }
            string customername = tbCustomerName.Text;
            string customeraddress = tbCustomerAddress.Text;
            int customerphone;
            if (!Int32.TryParse(tbCustomerPhone.Text, out customerphone))
            {
                MessageBox.Show("Internal error: integer value invalid " + tbCustomerPhone.Text);
                return;
            }


            if (Globals.currOrder == null)
            {
                
                    Order o = new Order
                    {
                        OrderId = Globals.currArtWork.ArtWorkId,
                        OrderDate = orderdate,
                        Status = status,
                        Notes = notes
                        //CustomerId = Globals.currCustomer.CustomerId
                    };
                    Customer a = new Customer
                    {
                        CustomerName = customername,
                        CustomerAddress = customeraddress,
                        CustomerPhone = customerphone


                    };

                    o.CustomerId = a.CustomerId;
                    Globals.ctx.Orders.Add(o);
                    Globals.ctx.Customers.Add(a);
                    Globals.ctx.SaveChanges();
                }

            
          
            else
            {
                Globals.currOrder.OrderDate = orderdate;
                Globals.currOrder.Notes = notes;
                Globals.currOrder.Status = status;
                Globals.currOrder.Customer.CustomerName = customername;
                Globals.currOrder.Customer.CustomerAddress = customeraddress;
                Globals.currOrder.Customer.CustomerPhone = customerphone;
                Globals.ctx.SaveChanges();
            }


            MainWindow mw = new MainWindow();
            mw.RefreshList();
            this.DialogResult = true;

        }
        private void bnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

       //generate pdf file and print this page
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDlg = new PrintDialog();
            
            printDlg.PrintVisual(this, "Window Printing.");

        }


    }
}




