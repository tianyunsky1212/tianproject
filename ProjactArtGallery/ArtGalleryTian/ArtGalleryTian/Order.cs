﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryTian
{
    public class Order
    {
        [ForeignKey("ArtWork")]
        public int OrderId { get; set; }

        // one-to-Zero-or-One relationship between ArtWork and Order
        public virtual ArtWork ArtWork { get; set; }

        //[ForeignKey("ArtWorks")]
        //public  int Id { get; set; }

       
        // for one to many relationship between  Customer and Order
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        

        [Required]
        public string Notes { get; set; }

        

        [Required]
        public StatusEnum Status { get; set; }

        public enum StatusEnum { Delivered = 0, Processing = 1, Pending = 2 }



        public override string ToString()
        {
            return string.Format("Order {0} placed on {1} , status is  {2} , Notes: {3} ",
                 OrderId, OrderDate, Status ,Notes);
        }
    }
}
