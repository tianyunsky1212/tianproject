﻿using ArtGalleryTian.Properties;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ArtGalleryTian
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
   
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            InitializeComponent();
            Globals.ctx = new ArtGalleryDatabaseContext();
            Globals.currArtWork = lvArtWorks.SelectedItem as ArtWork;          
            RefreshList();
        }


       public void RefreshList()
        {
           
            try
            {
                //listview Artworks
                List<ArtWork> list = (from p in Globals.ctx.ArtWorks select p).ToList();
                lvArtWorks.ItemsSource =list;
                // lvArtists
                List<Artist> artistslist = (from p in Globals.ctx.Artists select p).ToList();
                lvArtists.ItemsSource = artistslist;
                //lvOrders
                {
                    List<Order> list1 = (from p in Globals.ctx.Orders select p).ToList();
                    lvOrders.ItemsSource = list1;
                }
                //listview customers
                List<Customer> listcustomer = (from p in Globals.ctx.Customers select p).ToList();
                lvCustomer.ItemsSource = listcustomer;
                Globals.currArtWork = lvArtWorks.SelectedItem as ArtWork;
                /*
                else
                {
                    var orderlist = (from p in Globals.ctx.Orders
                                     join t in Globals.ctx.ArtWorks on p.OrderId equals t.ArtWorkId
                                     select new
                                     {
                                         ArtWorkName = t.ArtWorkName,
                                         OrderId = t.ArtWorkId,
                                         ArtWorkPrice = t.ArtWorkPrice,
                                         OrderDate = p.OrderDate,
                                         Status = p.Status,
                                         Notes = p.Notes,
                                         CustomerName = p.CustomerName
                                     }
                        ).ToList();
                    lvOrders.ItemsSource = orderlist;

                }
                */

            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to database:\n" + ex.Message, "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }

        }

       
        private void bnNewArtWorkRecord_Click(object sender, RoutedEventArgs e)
        {
            ArtWorksDialog dlg = new ArtWorksDialog(this);
            if (dlg.ShowDialog() == true)
            {
                RefreshList();
                MessageBox.Show("Success to add a new record!");
              
            }

        }

        private void lvArtWorks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Globals.currArtWork = lvArtWorks.SelectedItem as ArtWork;
            if (Globals.currArtWork == null)
            {
                return;
            }

            ArtWorksDialog dlg = new ArtWorksDialog(this, Globals.currArtWork);
            if (dlg.ShowDialog() == true)
            {
                RefreshList();
                MessageBox.Show("Success!");

            }
        }

        private void lvArtists_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           
            Globals.currArtist = lvArtists.SelectedItem as Artist;
            Globals.currArtWork = Globals.ctx.ArtWorks.Find(Globals.currArtist.ArtistId);
            if (Globals.currArtist == null)
            {
                return;
            }

            ArtWorksDialog dlg = new ArtWorksDialog(this, Globals.currArtWork);
            if (dlg.ShowDialog() == true)
            {
                RefreshList();
                MessageBox.Show("Added one artwork.");
            }
        }

        /*
        private void bnNewOrder_Click(object sender, RoutedEventArgs e)
        {

            Globals.currArtWork = lvArtWorks.SelectedItem as ArtWork;
            if (lvOrders.SelectedItem != null)
            {
                
                Globals.currOrder = lvOrders.SelectedItem as Order;
                Globals.currArtWork = Globals.ctx.ArtWorks.Find(Globals.currOrder.OrderId);
            }
            if (Globals.currArtWork == null)
            {
                return;
            }

            else
            {
                NewOrder dlg = new NewOrder(this,Globals.currArtWork,Globals.currOrder);
                if (dlg.ShowDialog() == true)//not use "show"
                {
                    RefreshList();
                    MessageBox.Show("Add an order!");

                }
            }
        }
        */

        private void miDetail_Click(object sender, RoutedEventArgs e)
        {
            Globals.currArtWork = lvArtWorks.SelectedItem as ArtWork;
            if (Globals.currArtWork != null)
            {

                MemoryStream memoryStream = new MemoryStream(Globals.currArtWork.ArtWorkImage);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
                string imagePath = Globals.currArtWork.ArtWorkImage.ToString();
                imageArtWorkmain.Source = bitmapImage; ;// bitmapImage;
                lbArtist.Content = Globals.currArtWork.Artist.ArtistName.ToString();
                lbArtWork.Content = Globals.currArtWork.ArtWorkName.ToString();
                lbArtWorkDes.Content = Globals.currArtWork.ArtWorkDescription.ToString();
                lbDate.Content = Globals.currArtWork.ArtWorkDate.Year.ToString();
            }
        }


        private void miPdf_Click(object sender, RoutedEventArgs e)
        {

            FileStream fs = new FileStream("OrderDetail.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            using (Globals.ctx)
            {
                Globals.currOrder = lvOrders.SelectedItem as Order;
                string orderpdf = "Customer Name: " + Globals.currOrder.Customer.CustomerName.ToString() + "\n"
                    + "Order ID: " + Globals.currOrder.OrderId.ToString() + "\n"
                    + "Order date: " + Globals.currOrder.OrderDate.ToString() + "\n"
                    + "Art work name: " + Globals.currOrder.ArtWork.ArtWorkName.ToString() + "\n"
                    + "Artist name: " + Globals.currOrder.ArtWork.Artist.ArtistName + "\n"
                    + "Order total: " + Globals.currOrder.ArtWork.ArtWorkPrice.ToString() + "\n"
                    + "Order status: " + Globals.currOrder.Status.ToString() + "\n"
                    + "Order details: " + Globals.currOrder.Notes.ToString()
                    ;
                doc.Add(new iTextSharp.text.Paragraph(orderpdf));
                doc.Close();
            }
        }

        private void miOrder_Click(object sender, RoutedEventArgs e)
        {

            Globals.currArtWork = lvArtWorks.SelectedItem as ArtWork;
            if (Globals.currArtWork == null)
            {
                return;
            }
       
            else
            {
                NewOrder dlg = new NewOrder(this, Globals.currArtWork, Globals.currOrder);
                if (dlg.ShowDialog() == true)//not use "show"
                {
                    RefreshList();
                    MessageBox.Show("Add an order!");

                }
            }
        }

        private void lvOrders_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           
            Globals.currOrder = lvOrders.SelectedItem as Order;
            Globals.currArtWork = Globals.ctx.ArtWorks.Find(Globals.currOrder.OrderId);
            if (Globals.currOrder == null)
            {
                return;
            }

            NewOrder dlg = new NewOrder(this, Globals.currArtWork,Globals.currOrder);
            if (dlg.ShowDialog() == true)
            {
                RefreshList();
                lblStatus.Content = "Added one order.";
            }
        }
        /*
        private void lvCustomer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Globals.currCustomer = lvCustomer.SelectedItem as Customer;
            
            //Globals.currOrder = Globals.ctx.Orders.Find(Globals.currArtWork.Order.OrderId) as Order;
            Globals.currArtWork = Globals.ctx.ArtWorks.Find(Globals.currOrder.OrderId) as ArtWork;
            if (Globals.currCustomer == null)
            {
                return;
            }

            NewOrder dlg = new NewOrder(this, Globals.currArtWork,Globals.currOrder);
            if (dlg.ShowDialog() == true)
            {
                RefreshList();
                lblStatus.Content = "Added one customer.";
            }

        }
        */

    }
}
