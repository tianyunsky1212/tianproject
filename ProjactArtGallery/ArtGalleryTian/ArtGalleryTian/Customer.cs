﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryTian
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }

        // for one to many relationship between Artiest and ArtWork
        public ICollection<Order> Orders { get; set; }

        [Required, MaxLength(500)]
        public string CustomerName { get; set; }

       
        [Required, MaxLength(500)]
        public string CustomerAddress { get; set; }

        [Required]
        public int CustomerPhone { get; set; }


        public override string ToString()
        {
            return string.Format("Created by Customer:id{0} ,name{1} ,address {2}, phone {3} ",
                 CustomerId, CustomerName, CustomerAddress, CustomerPhone);
        }
    }
}



