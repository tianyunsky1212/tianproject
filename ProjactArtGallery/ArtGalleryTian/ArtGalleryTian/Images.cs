﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace ArtGalleryTian
{
    public static class Images
    {
        /*
       public static byte[] ImageToByteArray(this System.Drawing.Image ArtWorkImage)
       {
           using (var ms = new MemoryStream())
           {
               ArtWorkImage.Save(ms, ArtWorkImage.RawFormat);
               return ms.ToArray();
           }
       }
       */
        public static byte[] ImageToByteArray(System.Drawing.Image ArtWorkImage)
        {
            using (var ms = new MemoryStream())
            {
                ArtWorkImage.Save(ms, ArtWorkImage.RawFormat);
                return ms.ToArray();
            }
        }
    }
}


