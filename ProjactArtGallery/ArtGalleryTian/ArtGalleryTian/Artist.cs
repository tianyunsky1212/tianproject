﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryTian
{
   public class Artist
    {
        [Key]
        public int ArtistId { get; set; }

        // for one to many relationship between Artiest and ArtWork
        public ICollection<ArtWork> ArtWorks { get; set; }

        [Required]
        public ArtistEnum ArtistName { get; set; }

        public enum ArtistEnum { Monet = 0, Picasso = 1, ZhangXiaoGang = 2, AndyWarhol = 3, Sisley = 4, EdgarDegas = 5, HenriMatisse = 6, ZhangDaqian = 7, YayoiKusama = 8 }


        [MaxLength(500)]
        public string ArtistDescription { get; set; }

        //[ForeignKey("ArtWorks")]
        //public int ArtWorkId { get; set; }



        public override string ToString()
        {
            return string.Format("Created by Artist:id{0} ,name{1} ,{2} ",
                 ArtistId.ToString(), ArtistName, ArtistDescription);
        }
    }
}
