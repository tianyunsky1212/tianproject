﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryTian
{
    class Globals
    {
        public static ArtGalleryDatabaseContext ctx = new ArtGalleryDatabaseContext();
        public static ArtWork currArtWork;
        public static Artist currArtist;
        public static Order currOrder;
        public static Customer currCustomer;
    }
}